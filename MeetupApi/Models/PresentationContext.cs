using Microsoft.EntityFrameworkCore;

namespace MeetupApi.Models
{
    public class PresentationContext : DbContext
    {
        public PresentationContext(DbContextOptions<PresentationContext> options)
            : base(options)
        {
        }

        public DbSet<PresentationItem> PresentationItems { get; set; }
    }
}