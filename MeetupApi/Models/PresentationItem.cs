using System.ComponentModel.DataAnnotations;


namespace MeetupApi.Models
{
    public class PresentationItem
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public System.DateTime? Date { get; set; }
        public bool? IsComplete { get; set; }
        public int? Rating { get; set; }
    }
}