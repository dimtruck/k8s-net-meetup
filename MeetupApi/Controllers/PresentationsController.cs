using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using MeetupApi.Models;

namespace MeetupApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    // [ApiController]
    public class PresentationsController : ControllerBase
    {
        private readonly PresentationContext _context;

        public PresentationsController(PresentationContext context)
        {
            _context = context;
        }       

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PresentationItem>), 200)]
        public IActionResult GetAll()
        {
            return Ok(_context.PresentationItems.ToList());
        }

        [HttpGet("{id}", Name = "GetPresentation")]
        [ProducesResponseType(typeof(PresentationItem), 200)]
        [ProducesResponseType(404)]
        public IActionResult GetById(long id)
        {
            var item = _context.PresentationItems.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpPost]
        [ProducesResponseType(typeof(PresentationItem), 201)]
        [ProducesResponseType(400)]
        public IActionResult Create([FromBody] PresentationItem item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!item.IsComplete.HasValue)
                item.IsComplete = false;
            _context.PresentationItems.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetPresentation", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(PresentationItem), 204)]
        [ProducesResponseType(404)]
        public IActionResult Update(long id, [FromBody] PresentationItem item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var presentation = _context.PresentationItems.Find(id);
            if (presentation == null)
            {
                return NotFound();
            }

            if (item.IsComplete.HasValue)
                presentation.IsComplete = item.IsComplete;
            if (!string.IsNullOrEmpty(item.Name))
                presentation.Name = item.Name;
            if (item.Date.HasValue)
                presentation.Date = item.Date;
            if (!string.IsNullOrEmpty(item.Author))
                presentation.Author = item.Author;
            if (item.Rating.HasValue)
                presentation.Rating = item.Rating;

            _context.PresentationItems.Update(presentation);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}/complete")]
        [ProducesResponseType(typeof(PresentationItem), 204)]
        [ProducesResponseType(404)]
        public IActionResult Complete(long id)
        {
            var presentation = _context.PresentationItems.Find(id);
            if (presentation == null)
            {
                return NotFound();
            }

            presentation.IsComplete = true;

            _context.PresentationItems.Update(presentation);
            _context.SaveChanges();
            return NoContent();
        }
    }
}